FROM php:7.4.33-apache-buster

COPY src /var/www/html
RUN chmod -R 777 /var/www/html

RUN apt update && apt install cron -y

COPY container_scripts /container_scripts
RUN chmod -R 700 /container_scripts
#Health Check script for prometheus / metriks
RUN crontab -l | { cat; echo "*/1 * * * * bash /container_scripts/health-check.sh"; } | crontab -

EXPOSE 80
CMD service apache2 start; bash /container_scripts/health-check.sh