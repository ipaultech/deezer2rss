# Deezer Artist 2 RSS

I've created this project to follow my favourite artists via RSS Feeds (for [FreshRSS](https://github.com/FreshRSS/FreshRSS)).
This App use the [Deezer API](https://developers.deezer.com/api) and make it accessible via RSS.

Use a Deezer Artist ID to generate the feed.
For example:
``YOURHOST?artist_id=1956``

![Screenshot #1](/screenshot1.png)

# Installation

You can deploy this project with docker-compose.

``git clone https://gitlab.com:ipaultech/deezer2rss.git``

And run it with...
``docker compose up -d``

Now you can access the RSS Feed with
``http://YOURHOST?artist_id=1956``

# Metriks / Uptime / Prometheus Exporter

This project has a custom Prometheus Exporter,
so you can monitor if all is going well with the Deezer API & the RSS Requests. 

You can access the metrik endpoint with:
``http://YOURHOST/metrik.php``

And here is a example prometheus exporter:
````
  - job_name: 'deezertorss'
    static_configs:
      - targets: ['YOURHOST:80']
    metrics_path: /metrik.php
````


