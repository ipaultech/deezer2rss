<?php
#Load sql.php for metrics
require __DIR__ . '/sql.php';

#Get parameter
$global_artist_id = $_GET['artist_id'];;
$api_test = 0;

#For automatic api testing
if ($argv[1] != null) {
   $global_artist_id = $argv[1];
   $api_test = 1;
}

$apihost = 'https://api.deezer.com';
#Search query URLs
$search_yt = 'https://q.berz.dev?q=!yt%20';
$search_ytmusic = 'https://q.berz.dev?q=!youtubemusic%20';
$search_qobuz = 'https://q.berz.dev?q=!qobuz%20';

#Error removing
error_reporting(E_ERROR | E_PARSE);

#API function
function callAPI($method, $url, $data){
    $curl = curl_init();
    switch ($method){
       case "POST":
          curl_setopt($curl, CURLOPT_POST, 1);
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
          break;
       case "PUT":
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
          break;
       default:
          if ($data)
             $url = sprintf("%s?%s", $url, http_build_query($data));
    }
    #Get variables
    global $apikey;
    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
       $apikey,
       'accept: application/json',
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    // EXECUTE:
    $result = curl_exec($curl);
    if(!$result){die("Connection Failure");}
    curl_close($curl);
    return $result;
 }

#API call functions
#Get artist information
function get_artist_information () {
    global $apihost;
    global $global_artist_id;
    global $artist_data;
    $get_data = callAPI('GET', $apihost .'/artist/' .$global_artist_id, false);
    $artist_data = json_decode($get_data, true);
}

#Get albums per Artist
function get_artist_album () {
    global $apihost;
    global $global_artist_id;
    global $artist_album_data;
    $get_data = callAPI('GET', $apihost .'/artist/' .$global_artist_id .'/albums', false);
    $artist_album_data = json_decode($get_data, true);
}

#Get albums tracks
function get_album_tracks ($aid) {
    global $apihost;
    global $album_tracks_data;
    $get_data = callAPI('GET', $apihost .'/album/' .$aid .'/tracks', false);
    $album_tracks_data = json_decode($get_data, true);
}

#Convert seconds to m:s
function durationconvert($iSeconds)
{
   $min = intval($iSeconds / 60);
   return $min . ':' . str_pad(($iSeconds % 60), 2, '0', STR_PAD_LEFT);
}

function log_plus_one($metricname) {
   global $api_test;
   if ($api_test == 0) {
      $current_metric = sql_get($metricname);
      $newmetric = $current_metric + 1;
      sql_set($metricname, $newmetric);
   }
}

if ($global_artist_id == null) {
   #Log this and exit!
   log_plus_one('requests_without_artistid');
   
   exit('please use a ?artist_id= quest.');
} else {
   #Log request to total count
   log_plus_one('request_count');
}

get_artist_information();
#Set artist information
$artist_name = $artist_data[name];
$artist_link = $artist_data[link];

#Test for failure
if ($artist_link == null ) {
   log_plus_one('total_error_count');
}

#Print RSS Head
header( "Content-type: text/xml");
echo "<?xml version='1.0' encoding='UTF-8'?>
<rss version='2.0'>
<channel>
<title>" .$artist_name ."</title>
<link>" .$artist_link ."</link>
<description>Artist Feed from " .$artist_name ." on Deezer</description>
<language>en-us</language>";

#Get album information
get_artist_album();
foreach($artist_album_data[data] as $item) {
    #Set vars
    $album_id = $item[id];
    $title = $item[title];
    $title = preg_replace("/[^A-Za-z0-9 ]/", '', $title);
    $preview_img = $item[cover_xl];
    $deezer_link = $item[link];
    $release_date = $item[release_date];

    #Get tracks from the selected album
    get_album_tracks($album_id);

    #Init counting vars & array
    $track_array = array();
    $track_count = 0;
    $track_duration_count = 0;
    foreach($album_tracks_data[data] as $item) {
        #Get Vars
        $track_title = $item[title];
        $track_duration = $item[duration];
        
        #Count stuff
        $track_duration_count += $track_duration;
        $track_count += 1;

        #Normalize Track Name
        $track_title = preg_replace("/[^A-Za-z0-9 ]/", '', $track_title);
        $track_duration = durationconvert($track_duration);

        #Set Track Title
        $track_title = $track_title .' [' .$track_duration .']';

        #And push it to array...
        array_push($track_array, $track_title);
    }
    #Convert duration of all tracks to the correct rss format
    $track_duration_count = durationconvert($track_duration_count);

    #Convert release date ISO 8601 to rfc-822
    $date = $release_date;
    $fixedrdate = date('r', strtotime(substr($date,0,10)));

    #search titel cleaning (replace " " with "%20")
    $search_titel = str_replace(' ', '%20', $title);
    $search_artist = str_replace(' ', '%20', $artist_name);

    #Print RSS Item
    echo "<item>
    <title>" .$title ."</title>
    <link>" .$deezer_link ."</link>
    <pubDate>" .$fixedrdate ."</pubDate>
    <description>" ;
    #Print description
    echo $track_count .' Tracks (' .$track_duration_count .' play time):';
    foreach($track_array as $track) {
        echo '&lt;br&gt;' .$track;
    }
    echo '&lt;br&gt;';
    echo '&lt;a href=' .$preview_img .'>Cover&lt;/a&gt;';
    echo '&lt;br&gt;';
    echo '&lt;a href=' .$search_yt .$search_artist .'%20' .$search_titel .'>=> YouTube&lt;/a&gt;';
    echo '&lt;br&gt;';
    echo '&lt;a href=' .$search_ytmusic .$search_artist .'%20' .$search_titel .'>=> YouTube Music&lt;/a&gt;';
    echo '&lt;br&gt;';
    echo '&lt;a href=' .$search_qobuz .$search_artist .'%20' .$search_titel .'>=> Qobuz&lt;/a&gt;';
    echo "</description>
    </item>";
}

#Print RSS ending
echo "</channel></rss>";

#Log if failed succeed or succeded quest
if ($track_count == null) {
   log_plus_one('tracklist_error_count');
} else {
   log_plus_one('succeded_request_count');
}

#IF Api Test
if ($api_test == 1 && $track_count != null) {
   sql_set('api_health_state', '1');
} elseif ($api_test == 1 && $track_count == null) {
   sql_set('api_health_state', '0');
}

?>
