<?php
#Load sql.php
require __DIR__ . '/sql.php';

$exporter_name = 'deezertorss_';

function convert_to_gauge($number) {
   return number_format((float)$number, 1, '.', '');

}

#Function to generate prometheus entry to text file
function gen_prometheus($endname, $value, $countertype) {
   global $exporter_name;
   global $edit_file;
   $istring = $exporter_name .$endname;
   $countstr = convert_to_gauge($value);

   #Type
   $twrite = '# TYPE ' .$istring .' ' .$countertype;
   echo($twrite ."<br>\n");

   #Values
   $twrite = $istring .' ' .$countstr;
   echo($twrite ."<br>\n");
}

#Get current values and print it to file
$metrics = array("succeded_request_count", "tracklist_error_count", "request_count", "total_error_count", "requests_without_artistid", "api_health_state");
foreach($metrics as $metric_item) {
   gen_prometheus($metric_item, sql_get($metric_item), 'gauge');
}
?>
