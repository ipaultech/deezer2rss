<?php
#Load sql.php
require __DIR__ . '/sql.php';

$metricfile = "metrik.txt";
$exporter_name = 'deezertorss_';

function convert_to_gauge($number) {
   return number_format((float)$number, 1, '.', '');

}

#Function to generate prometheus entry to text file
function gen_prometheus($endname, $value, $countertype) {
   global $metricfile;
   global $exporter_name;
   global $edit_file;
   $istring = $exporter_name .$endname;
   $countstr = convert_to_gauge($value);

   #Write to text file

   #Type
   $twrite = '# TYPE ' .$istring .' ' .$countertype. "\n";
   fwrite($edit_file, $twrite);
   
   #Values
   $twrite = $istring .' ' .$countstr. "\n";
   fwrite($edit_file, $twrite);
}

#Delete old metric file
unlink($metricfile);
#Create new file
$edit_file = fopen($metricfile, 'w');

#Get current values and print it to file
$metrics = array("succeded_request_count", "tracklist_error_count", "request_count", "total_error_count", "requests_without_artistid", "api_health_state");
foreach($metrics as $metric_item) {
   gen_prometheus($metric_item, sql_get($metric_item), 'gauge');
}

#Close metric txt file
fclose($edit_file);

#Show file (for prometheus)
header('Content-Type: text/plain');
header('Content-Disposition: inline; filename="metrik.txt"');
readfile($metricfile);
?>
